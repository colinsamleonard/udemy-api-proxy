terraform {
  backend "s3" {
    bucket         = "udemy-app-devops-tfstate"
    key            = "udemy-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "udemy-app-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"

  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
