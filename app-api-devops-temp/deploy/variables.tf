variable "prefix" {
  default = "uaad"
}

variable "project" {
  default = "udemy-app-api-devops"
}

variable "contact" {
  default = "me"
}